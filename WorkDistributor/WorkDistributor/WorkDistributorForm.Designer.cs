﻿namespace WorkDistributor
{
    partial class WorkDistributorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.DGV_Tables = new System.Windows.Forms.DataGridView();
            this.txtsplitsize = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBytes = new System.Windows.Forms.Label();
            this.btn_Generate = new System.Windows.Forms.Button();
            this.lblKB = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMB = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbCompFile = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbCompIndx = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbCompAct = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbCompModel = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbProcessAtt = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbGenstatistic = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbEncryptIDX = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbEncryptAF = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbRpt = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbAccDef = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Tables)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DGV_Tables);
            this.panel1.Location = new System.Drawing.Point(12, 182);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 210);
            this.panel1.TabIndex = 2;
            // 
            // DGV_Tables
            // 
            this.DGV_Tables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Tables.Location = new System.Drawing.Point(3, 3);
            this.DGV_Tables.Name = "DGV_Tables";
            this.DGV_Tables.ReadOnly = true;
            this.DGV_Tables.Size = new System.Drawing.Size(584, 203);
            this.DGV_Tables.TabIndex = 0;
            // 
            // txtsplitsize
            // 
            this.txtsplitsize.Location = new System.Drawing.Point(468, 401);
            this.txtsplitsize.Name = "txtsplitsize";
            this.txtsplitsize.Size = new System.Drawing.Size(113, 20);
            this.txtsplitsize.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(33, 405);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Total Size  Bytes : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(384, 405);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Size Split Bytes";
            // 
            // lblBytes
            // 
            this.lblBytes.AutoSize = true;
            this.lblBytes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBytes.Location = new System.Drawing.Point(137, 406);
            this.lblBytes.Name = "lblBytes";
            this.lblBytes.Size = new System.Drawing.Size(14, 13);
            this.lblBytes.TabIndex = 8;
            this.lblBytes.Text = "0";
            // 
            // btn_Generate
            // 
            this.btn_Generate.Location = new System.Drawing.Point(211, 489);
            this.btn_Generate.Name = "btn_Generate";
            this.btn_Generate.Size = new System.Drawing.Size(158, 23);
            this.btn_Generate.TabIndex = 12;
            this.btn_Generate.Text = "Generate";
            this.btn_Generate.UseVisualStyleBackColor = true;
            this.btn_Generate.Click += new System.EventHandler(this.btn_Generate_Click);
            // 
            // lblKB
            // 
            this.lblKB.AutoSize = true;
            this.lblKB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKB.Location = new System.Drawing.Point(137, 427);
            this.lblKB.Name = "lblKB";
            this.lblKB.Size = new System.Drawing.Size(14, 13);
            this.lblKB.TabIndex = 14;
            this.lblKB.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 427);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Total Size  KB: ";
            // 
            // lblMB
            // 
            this.lblMB.AutoSize = true;
            this.lblMB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMB.Location = new System.Drawing.Point(137, 450);
            this.lblMB.Name = "lblMB";
            this.lblMB.Size = new System.Drawing.Size(14, 13);
            this.lblMB.TabIndex = 16;
            this.lblMB.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(33, 450);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Total Size  MB: ";
            // 
            // cmbCompFile
            // 
            this.cmbCompFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCompFile.FormattingEnabled = true;
            this.cmbCompFile.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbCompFile.Location = new System.Drawing.Point(150, 12);
            this.cmbCompFile.Name = "cmbCompFile";
            this.cmbCompFile.Size = new System.Drawing.Size(121, 21);
            this.cmbCompFile.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "COMPRESSFILE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(293, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "COMPRESSINDEXFILE ";
            // 
            // cmbCompIndx
            // 
            this.cmbCompIndx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCompIndx.FormattingEnabled = true;
            this.cmbCompIndx.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbCompIndx.Location = new System.Drawing.Point(428, 12);
            this.cmbCompIndx.Name = "cmbCompIndx";
            this.cmbCompIndx.Size = new System.Drawing.Size(121, 21);
            this.cmbCompIndx.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(293, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "COMPRESSACTIVE ";
            // 
            // cmbCompAct
            // 
            this.cmbCompAct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCompAct.FormattingEnabled = true;
            this.cmbCompAct.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbCompAct.Location = new System.Drawing.Point(428, 46);
            this.cmbCompAct.Name = "cmbCompAct";
            this.cmbCompAct.Size = new System.Drawing.Size(121, 21);
            this.cmbCompAct.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "COMPRESSMODEL ";
            // 
            // cmbCompModel
            // 
            this.cmbCompModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCompModel.FormattingEnabled = true;
            this.cmbCompModel.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cmbCompModel.Location = new System.Drawing.Point(150, 46);
            this.cmbCompModel.Name = "cmbCompModel";
            this.cmbCompModel.Size = new System.Drawing.Size(121, 21);
            this.cmbCompModel.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(293, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "PROCESS_FILEATTACH";
            // 
            // cmbProcessAtt
            // 
            this.cmbProcessAtt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProcessAtt.FormattingEnabled = true;
            this.cmbProcessAtt.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbProcessAtt.Location = new System.Drawing.Point(428, 121);
            this.cmbProcessAtt.Name = "cmbProcessAtt";
            this.cmbProcessAtt.Size = new System.Drawing.Size(121, 21);
            this.cmbProcessAtt.TabIndex = 31;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "GENSTATISTIC";
            // 
            // cmbGenstatistic
            // 
            this.cmbGenstatistic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGenstatistic.FormattingEnabled = true;
            this.cmbGenstatistic.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbGenstatistic.Location = new System.Drawing.Point(150, 121);
            this.cmbGenstatistic.Name = "cmbGenstatistic";
            this.cmbGenstatistic.Size = new System.Drawing.Size(121, 21);
            this.cmbGenstatistic.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(293, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "ENCRYPTIDX";
            // 
            // cmbEncryptIDX
            // 
            this.cmbEncryptIDX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEncryptIDX.FormattingEnabled = true;
            this.cmbEncryptIDX.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbEncryptIDX.Location = new System.Drawing.Point(428, 87);
            this.cmbEncryptIDX.Name = "cmbEncryptIDX";
            this.cmbEncryptIDX.Size = new System.Drawing.Size(121, 21);
            this.cmbEncryptIDX.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(37, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "ENCRYPTAF";
            // 
            // cmbEncryptAF
            // 
            this.cmbEncryptAF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEncryptAF.FormattingEnabled = true;
            this.cmbEncryptAF.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbEncryptAF.Location = new System.Drawing.Point(150, 87);
            this.cmbEncryptAF.Name = "cmbEncryptAF";
            this.cmbEncryptAF.Size = new System.Drawing.Size(121, 21);
            this.cmbEncryptAF.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(37, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "CREATEREPORT";
            // 
            // cmbRpt
            // 
            this.cmbRpt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRpt.FormattingEnabled = true;
            this.cmbRpt.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbRpt.Location = new System.Drawing.Point(150, 157);
            this.cmbRpt.Name = "cmbRpt";
            this.cmbRpt.Size = new System.Drawing.Size(121, 21);
            this.cmbRpt.TabIndex = 33;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(293, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(117, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "ACCESS DEFINATION";
            // 
            // cmbAccDef
            // 
            this.cmbAccDef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAccDef.FormattingEnabled = true;
            this.cmbAccDef.Items.AddRange(new object[] {
            "LOCALAD ",
            "NAMEDAD"});
            this.cmbAccDef.Location = new System.Drawing.Point(428, 157);
            this.cmbAccDef.Name = "cmbAccDef";
            this.cmbAccDef.Size = new System.Drawing.Size(121, 21);
            this.cmbAccDef.TabIndex = 35;
            // 
            // WorkDistributorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 524);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cmbAccDef);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cmbRpt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cmbProcessAtt);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cmbGenstatistic);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmbEncryptIDX);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cmbEncryptAF);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbCompAct);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmbCompModel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbCompIndx);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCompFile);
            this.Controls.Add(this.lblMB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblKB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_Generate);
            this.Controls.Add(this.lblBytes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtsplitsize);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "WorkDistributorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work Distributor";
            this.Load += new System.EventHandler(this.WorkDistributorForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Tables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView DGV_Tables;
        private System.Windows.Forms.TextBox txtsplitsize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBytes;
        private System.Windows.Forms.Button btn_Generate;
        private System.Windows.Forms.Label lblKB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbCompFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbCompIndx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbCompAct;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbCompModel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbProcessAtt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbGenstatistic;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbEncryptIDX;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbEncryptAF;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbRpt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbAccDef;
    }
}

