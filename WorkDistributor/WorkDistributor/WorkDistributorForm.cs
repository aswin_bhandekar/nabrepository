﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorkDistributorBL;

namespace WorkDistributor
{
    public partial class WorkDistributorForm : Form
    {
        public WorkDistributorForm()
        {
            InitializeComponent();
        }

        private void WorkDistributorForm_Load(object sender, EventArgs e)
        {
            DataSet DS = new DataSet();
            DS = WorkDistributorBL.WorkDistributorBL.GetDataBaseSizeDetails();
            string strDBName = DS.Tables[0].Rows[0]["database_name"].ToString();
            string strDBSize = DS.Tables[0].Rows[0]["database_size"].ToString();
            string strunallocatedspace = DS.Tables[0].Rows[0]["unallocated space"].ToString();
            string strReserved = DS.Tables[1].Rows[0]["reserved"].ToString();
            string strdata = DS.Tables[1].Rows[0]["data"].ToString();
            string strindexsize = DS.Tables[1].Rows[0]["index_size"].ToString();
            string strUnused = DS.Tables[1].Rows[0]["unused"].ToString();

            //lblDBDetails.Text = "Database Name : " + strDBName + " Size : " + strDBSize + " Unallocated Space : " + strunallocatedspace + " Reserved : " + strReserved + "";
            //lblDBdatadetails.Text = " Data : " + strdata + " Index Size : " + strindexsize + " UnUsed : " + strUnused + "";


            cmbCompFile.Text = "YES";
            cmbCompIndx.Text = "YES";
            cmbCompModel.Text = "2";
            cmbCompAct.Text = "NO";
            cmbEncryptAF.Text = "NO";
            cmbEncryptIDX.Text = "NO";
            cmbGenstatistic.Text = "YES";
            cmbProcessAtt.Text = "YES";
            cmbRpt.Text = "NO";
            cmbAccDef.Text = "LOCALAD";

            GetTableSpaceUsed();

        }

        private void GetTableSizeData()
        {
            DataSet Ds_Tables = new DataSet();
            Ds_Tables = WorkDistributorBL.WorkDistributorBL.GetTablesSizeDetails();
            DGV_Tables.DataSource = Ds_Tables.Tables[0];
        }

        private void GetTablesDataSizeDetails()
        {
            DataSet Ds_Tabledatasize = new DataSet();
            Ds_Tabledatasize = WorkDistributorBL.WorkDistributorBL.GetTablesDataSizeDetails();
            DGV_Tables.DataSource = Ds_Tabledatasize.Tables[0];
            decimal fltTotalSizeMB = 0;
            decimal fltTotalSizeGB = 0;

            foreach (DataRow DR in Ds_Tabledatasize.Tables[0].Rows)
            {
                fltTotalSizeMB = fltTotalSizeMB + decimal.Round(Convert.ToDecimal(DR["SizeMB"]),2);
                fltTotalSizeGB = fltTotalSizeGB + decimal.Round(Convert.ToDecimal(DR["SizeGB"]),2);
            }
            lblBytes.Text = fltTotalSizeMB.ToString();
            //lbltotalsizeGB.Text = fltTotalSizeGB.ToString();
        }
        
        private void GetTableSpaceUsed()
        {
            DataSet Ds_Tabledatasize = new DataSet();
            Ds_Tabledatasize = WorkDistributorBL.WorkDistributorBL.GetTableSpaceUsed();
            DGV_Tables.DataSource = Ds_Tabledatasize.Tables[0];

            lblBytes.Text = Ds_Tabledatasize.Tables[1].Rows[0]["Total Bytes"].ToString();
            lblKB.Text = ((Convert.ToDouble(Ds_Tabledatasize.Tables[1].Rows[0]["Total Bytes"])) * 0.001).ToString();
            lblMB.Text = ((Convert.ToDouble(lblKB.Text)) * 0.001).ToString();
        }
        private void GenerateLogic()
        {
            decimal fltTotalSizeMB = 0;
            decimal fltTotalSizeKB = 0;
            decimal fltTotalSizeByt = 0;
            Int64 fltsplitsize = 0;
            
            List<WorkDistributorEntity> objWDEntityList = new List<WorkDistributorEntity>();

            fltTotalSizeMB = decimal.Round(Convert.ToDecimal(lblMB.Text),2);
            fltTotalSizeByt = decimal.Round(Convert.ToDecimal(lblBytes.Text), 2);
            fltTotalSizeKB= decimal.Round(Convert.ToDecimal(lblKB.Text), 2);
           
            fltsplitsize = Convert.ToInt64(txtsplitsize.Text);
            //string strfromto = cmbfromto.SelectedText.ToString();

            if (DGV_Tables.Rows.Count - 1 > 0)
            {
                for (int i = 0; i < DGV_Tables.Rows.Count - 1; i++)
                {
                    WorkDistributorEntity objWDEntity = new WorkDistributorEntity();
                    objWDEntity.TableName = DGV_Tables.Rows[i].Cells[1].Value.ToString();
                    objWDEntity.TableRowCount = Convert.ToInt64(DGV_Tables.Rows[i].Cells[2].Value.ToString());
                    //objWDEntity.TableSizeKB = Convert.ToDecimal(DGV_Tables.Rows[i].Cells[3].Value.ToString());
                    objWDEntity.TableSizeByt =  Convert.ToInt64(DGV_Tables.Rows[i].Cells[4].Value.ToString());
                    objWDEntityList.Add(objWDEntity);
                }
            }
            List<WorkDistributorScripts> objSqlScriptslist = new List<WorkDistributorScripts>();
            //objSqlScriptslist = WorkDistributorBL.WorkDistributorBL.GenerateSQLScripts(objWDEntityList, fltsplitsize, Convert.ToInt64 (fltTotalSizeByt));

            WDFileFormatInputs objWDFileFormatInputs = new WDFileFormatInputs();
            objWDFileFormatInputs.COMPRESSFILE = cmbCompFile.Text.Substring(0, 1);
            objWDFileFormatInputs.COMPRESSINDEXFILE = cmbCompIndx.Text.Substring(0, 1);
            objWDFileFormatInputs.COMPRESSMODEL = Convert.ToInt16(cmbCompModel.Text);
            objWDFileFormatInputs.COMPRESSACTIVE = cmbCompAct.Text.Substring(0, 1);
            objWDFileFormatInputs.ENCRYPTAF = cmbEncryptAF.Text.Substring(0, 1);
            objWDFileFormatInputs.ENCRYPTIDX = cmbEncryptIDX.Text.Substring(0, 1);
            objWDFileFormatInputs.GENSTATISTIC = cmbGenstatistic.Text.Substring(0, 1);
            objWDFileFormatInputs.PROCESS_FILEATTACH = cmbProcessAtt.Text.Substring(0, 1);
            objWDFileFormatInputs.CREATEREPORT = cmbRpt.Text.Substring(0, 1);
            objWDFileFormatInputs.ACCESSDEFINATION = cmbAccDef.Text;

            List<string> strSqlscriptlist = new List<string>();
            string strfileoutput = "";
            strfileoutput = WorkDistributorBL.WorkDistributorBL.GenerateSQLScriptThroughQueries(fltsplitsize, objWDFileFormatInputs);
            
            

            //string FileName = strFileSavePath + "IISWebAppAssemblyDetails_" + string.Format("{0:yyyy-MM-dd_hh-mm-ss}", DateTime.Now) + ".xml";
            //System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<WorkDistributorScripts>));
            //using (TextWriter writer = new StreamWriter(FileName))
            //{
            //    serializer.Serialize(writer, strfileoutput);
            //}
            MessageBox.Show("XML Report has been generated.");
        }

        private void btn_Generate_Click(object sender, EventArgs e)
        {
            

            GenerateLogic();
        }
    }
}
