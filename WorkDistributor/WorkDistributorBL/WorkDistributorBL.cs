﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.IO;

namespace WorkDistributorBL
{
    public static class WorkDistributorBL
    {
        public static WDExistingSizeDetails objWDExistingSizeDetailsEntity = new WDExistingSizeDetails();
        public static Int64 intRRwsTotalSize = 0;
        public static string strFileNameOut = "";
        public static DataSet GetDataBaseSizeDetails()
        {
            DataSet DS = new DataSet();

            SqlParameter[] spparam = null;
            DS = DataBaseUtility.DataSet("sp_spaceused", spparam);
            return DS;
        }

        public static DataSet GetTablesSizeDetails()
        {
            DataSet DS = new DataSet();
            string StrQuery = "SELECT t.Name AS TableName,p.rows AS RowCounts, " +
                               "CAST(ROUND(((SUM(a.total_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS TotalSpaceMB, " +
                               "CAST(ROUND(((SUM(a.used_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS UsedSpaceMB, " +
                               "CAST(ROUND(((SUM(a.total_pages) - SUM(a.used_pages)) * 8) / 1024.00, 2) AS NUMERIC(36, 2)) AS UnusedSpaceMB " +
                               "FROM sys.tables t " +
                               "INNER JOIN sys.indexes i ON t.OBJECT_ID = i.object_id " +
                               "INNER JOIN sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id " +
                               "INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id " +
                               "INNER JOIN sys.schemas s ON t.schema_id = s.schema_id " +
                               "GROUP BY t.Name, s.Name, p.Rows " +
                               "ORDER BY s.Name, t.Name";
            DS = DataBaseUtility.DataSetText(StrQuery);
            return DS;
        }

        public static DataSet GetTablesDataSizeDetails()
        {
            DataSet Ds = new DataSet();
            string StrQuery = " declare @table nvarchar(128); " +
                              " declare @idcol nvarchar(128); " +
                              " declare @sql nvarchar(max); " +

                              " Declare @Tbl_Tablenames table(intslno int identity(1, 1), tbl_name varchar(100)) " +

                              " Insert into @Tbl_Tablenames(tbl_name) " +
                              " Select name from sys.tables " +

                              " Declare @intcount int= 0 " +
                              " select @intcount = count(intslno) from @Tbl_Tablenames " +
                              " Declare @i int= 0 " +
                              " Declare @Tbl_TableDataSize Table(intslno int identity(1, 1), Name Varchar(100), Size float) " +
                              " Declare @Tbl_Size Table(intslno int identity(1, 1), Size VARCHAR(MAX)) " +

                              " while @i < @intcount " +
                              " Begin " +
                              " set @i = @i + 1 " +
                              " select @table = RTRIM(tbl_name) from @Tbl_Tablenames where intslno = @i " +

                              " set @sql = 'select(0' " +
                              
                              " select @sql = @sql + ' + sum(isnull(datalength(' + name + '), 1))' " +
                              " from sys.columns where object_id = object_id(@table) " +
                              " set @sql = @sql + ') * 0.001 * 0.001 as RowSizeMB from ' + @table + ' order by rowsizemb desc' " +

                              " Insert into @Tbl_TableDataSize values (@table,0) " +

                              " Insert into @Tbl_Size " +
                              " exec (@sql) " +

                              " Update @Tbl_TableDataSize " +
                              " set Size = cast(T.size as float) " +
                              " From @Tbl_Size T Inner Join @Tbl_TableDataSize as D on T.intslno = D.intslno " +

                              " end " +
                              " select intslno As 'IntSlno', Name As 'Table Name', Size As 'SizeMB',cast(Size as decimal)*0.001 AS 'SizeGB' from @Tbl_TableDataSize ";
            Ds = DataBaseUtility.DataSetText(StrQuery);
            return Ds;
        }

        public static DataSet GetTableSpaceUsed()
        {
            DataSet DS = new DataSet();
            string StrQuery = " Declare @TempTableName Table (intslno int identity(1,1),txtTableName Varchar(100)) " +
                              " Declare @TableSpaceUsed Table(intslno int identity(1, 1), txtName Varchar(100), intRowcount BIGInt, txtReserved Varchar(MAX), txtData Varchar(MAX), txtIndexSize Varchar(Max), txtUnused Varchar(MAX)) " +
                              " Insert into @TempTableName " +
                              " select name from sys.tables " +

                              " Declare @intRowcount int " +
                              " Declare @intcountincr int= 0 " +

                              " Select @intRowcount = Count(1) from @TempTableName " +

                              " WHILE @intcountincr < @intRowcount " +
                              " BEGIN " +
                              " SET @intcountincr = @intcountincr + 1 " +
                              " Declare @TableName Varchar(100) " +
                              " SELECT @TableName = txtTableName FROM @TempTableName Where intslno = @intcountincr " +

                              " Insert INTO @TableSpaceUsed " +
                              " EXEC sp_spaceused @TableName " +
                              " END " +
                              
                              " SELECT intslno AS 'Slno',txtName AS 'Name',intRowcount AS 'Row Count', txtData AS 'Data KB', " +
                              " (Substring(txtData, 1, len(txtData) - 2) * 1000) AS 'Data Byte' FROM @TableSpaceUsed " +

                              " SELECT SUM((Substring(txtData, 1, len(txtData) - 2) * 1000)) AS 'Total Bytes' from @TableSpaceUsed ";
                              
                              
                DS = DataBaseUtility.DataSetText(StrQuery);
                return DS;
        }

        public static List<WorkDistributorScripts> GenerateSqlScripts(Int64 intsplitsize, decimal dctotalsizeMB,decimal dctotalsizeKB, decimal dctotalsizeByt, List<WorkDistributorEntity> WDTableSizeEntitylist)
        {
            List<WorkDistributorScripts> strSqlScriptsList = new List<WorkDistributorScripts>();
            GenerateSQLScripts(WDTableSizeEntitylist,intsplitsize, Convert.ToInt64(dctotalsizeByt));
            //string sqlscriptPrev = "";
            //Int64 intSlno = 1;
            //Int64 intRowNumPrev = 0;
            //Int64 intRowCountPrev = 0;
            //Int64 intRowSizePrev = 0;
            //string sqlscript = "";
            //Int64 intRemaingRows = 0;
            //WDRestRowsDetails objWDRestRowsDetails = new WDRestRowsDetails();
            //foreach (WorkDistributorEntity wbentity in WDTableSizeEntitylist)
            //{
            //    string strTableName = wbentity.TableName;

            //    Int64 intRowsforSplitSize = 0;
            //    WDRowsDetails objWDRowsDetailsEntity = new WDRowsDetails();
            //    objWDRowsDetailsEntity = CalculateRows(intsplitsize, dctotalsizeByt, wbentity.TableSizeByt, wbentity.TableRowCount);
            //    Int64 introwcountincr = 0;
            //    Int64 intTotalRows = wbentity.TableRowCount;

            //    Int64 intLoopsforTable = (wbentity.TableRowCount / objWDRowsDetailsEntity.IntRowsforSplitSize);

            //    for (Int64 i =1;i<= intTotalRows; i= i+ objWDRowsDetailsEntity.IntRowsforSplitSize)
            //    {
            //        introwcountincr = introwcountincr + objWDRowsDetailsEntity.IntRowsforSplitSize;
            //        if(introwcountincr < intTotalRows)
            //        {
            //            if(sqlscriptPrev !="")
            //            {
            //                Int64 IntRows = objWDRestRowsDetails.IntSizeNeededFormNextTable / objWDRowsDetailsEntity.IntEachRowSizeByt;
            //                introwcountincr = IntRows;

            //                sqlscriptPrev = "";

            //            }
            //            sqlscript = " SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from " + strTableName + ") AS foo " +
            //                    " WHERE RowNum>=" + i + " and RowNum<= " + introwcountincr + "";
            //        }
            //        else
            //        {

            //            objWDRestRowsDetails = CalculateRestRowSize(i, intTotalRows, objWDRowsDetailsEntity.IntEachRowSizeByt, intsplitsize);
            //            sqlscriptPrev = " SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from " + strTableName + ") AS foo " +
            //                  " WHERE RowNum>=" + i + " and RowNum<= " + intTotalRows + "";
            //        }
            //    }

            //}

            return strSqlScriptsList;
        }
        public static WDRowsDetails CalculateRows(Int64 intsplitsize, decimal dctotalsizeByt, decimal TableSizeByt, Int64 TableRowCount)
        {
            WDRowsDetails objwdrowsdetails = new WDRowsDetails();
            //objwdrowsdetails.IntRowCount = TableRowCount;
            //objwdrowsdetails.IntEachRowSizeByt = Convert.ToInt64(decimal.Round((TableSizeByt / TableRowCount), 2));
            //objwdrowsdetails.IntRowsforSplitSize = intsplitsize / objwdrowsdetails.IntEachRowSizeByt;
            return objwdrowsdetails;
        }
        public static WDRestRowsDetails CalculateMinValToGetRequiredSize(Int64 intNextrecords, Int64 intTotalRowcount, Int64 intEachRowSizebyt, Int64 intsplitsize)
        {
            WDRestRowsDetails objWDRestRowsDetails = new WDRestRowsDetails();
            //objWDRestRowsDetails.IntRemainingRows = intTotalRowcount - intNextrecords;
            //objWDRestRowsDetails.IntRemainingRowsSize = intEachRowSizebyt * objWDRestRowsDetails.IntRemainingRows;
            //objWDRestRowsDetails.IntSizeNeededFormNextTable = intsplitsize - objWDRestRowsDetails.IntRemainingRowsSize;
            return objWDRestRowsDetails;
        }

        public static bool IsTotalRequireSizelessthanTotalActualSizeofDataBase(Int64 intTRS,Int64 intDBsize)
        {
            bool blnIsLess = false;

            if(intTRS < intDBsize)
            {
                blnIsLess = true;
            }
            return blnIsLess;
        }
        public static bool IsEachTableSizelessthanTotalActualSizeofDataBase(Int64 intETS,Int64 intDBsize)
        {
            bool blnIsLess = false;

            if(intETS < intDBsize)
            {
                blnIsLess = true;
            }
            return blnIsLess;
        }
        public static Int64 CalculateEachRowSizeofTable(Int64 intTS, Int64 intTRwsCount)
        {
            Int64 intERwSize = intTS / intTRwsCount;
            return intERwSize;
        }
        public static WDExistingSizeDetails CalculateRestTotalRowsSize(string strTableName,Int64 intMinval,Int64 TRwsCount,Int64 intERwSize)
        {
            Int64 intVal = 0;
            Int64 intRestTotalRowsCount = TRwsCount - intMinval + 1;
            intRRwsTotalSize = intERwSize * intRestTotalRowsCount;
            objWDExistingSizeDetailsEntity = new WDExistingSizeDetails();
            objWDExistingSizeDetailsEntity.TableName = strTableName;
            objWDExistingSizeDetailsEntity.IntExistingSize = intRRwsTotalSize;
            objWDExistingSizeDetailsEntity.IntExistingRowscount = intRestTotalRowsCount;
            objWDExistingSizeDetailsEntity.IntExistingMinVal = intMinval;
            string sqlScriptExisting = " SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from " + objWDExistingSizeDetailsEntity.TableName + ") AS TableNameScript " +
                                         " WHERE RowNum>=" + intMinval + " and RowNum<= " + TRwsCount + "";
            objWDExistingSizeDetailsEntity.StrExistingSQLScript = sqlScriptExisting;

            return objWDExistingSizeDetailsEntity;
        }
        
        public static Int64 CalculateRequiredRowsFromTable(Int64 intTRS, Int64 intERS)
        {
            Int64 intMaxRowCount = 0;
            intMaxRowCount = intTRS / intERS;
            return intMaxRowCount;
        }
        public static List<WorkDistributorScripts> GenerateSQLScripts(List<WorkDistributorEntity> WDTableSizeEntitylist,Int64 intTRS, Int64 intTDBSize)
        {

            bool IsTRSLess = IsTotalRequireSizelessthanTotalActualSizeofDataBase(intTRS, intTDBSize);
            List<WorkDistributorScripts> strSqlScriptsList = new List<WorkDistributorScripts>();
            //if(IsTRSLess == true)
            //{
                Int64 intMinvalRest = 1;
                Int64 intTRwsCountRest =0;
                Int64 intRestTotalRowsSize = 0;
                string sqlScriptExisting = "";
            int intslno = 0;
            foreach (WorkDistributorEntity objWorkDistributorEntity in WDTableSizeEntitylist)
            {
                WorkDistributorScripts objWorkDistributorScriptsAll = new WorkDistributorScripts();
                //objWDExistingSizeDetailsEntity = null;
                Int64 intTS = objWorkDistributorEntity.TableSizeByt;
                Int64 intTRwsCount = objWorkDistributorEntity.TableRowCount;
                bool intTTSLess = IsEachTableSizelessthanTotalActualSizeofDataBase(intTS, intTDBSize);
                List<string> strscriptlistall = new List<string>();
                if (intTTSLess == true)
                {
                    Int64 intERwSize = CalculateEachRowSizeofTable(intTS, intTRwsCount);
                    Int64 intMaxval = CalculateRequiredRowsFromTable(intTRS, intERwSize);
                    Int64 intLoopCount = intTRwsCount / intMaxval;
                    Int64 intIncr = 0;
                    Int64 intMinVal = 1;
                    Int64 intRestrowsminval = 1;
                    Int64 intReqMaxVal = 0;
                    Int64 intMaxTemstore = 0;
                    if (objWDExistingSizeDetailsEntity != null)
                    {
                        if (objWDExistingSizeDetailsEntity.TableName != null)
                        {
                            Int64 intReqSize = intTRS - objWDExistingSizeDetailsEntity.IntExistingSize;
                            intReqMaxVal = intReqSize / intERwSize;
                        }
                    }
                    intMaxTemstore = intMaxval;
                    for (intMinVal = 1; intMinVal <= intTRwsCount; intMinVal = intMinVal + intMaxval)
                    {
                        objWorkDistributorScriptsAll = new WorkDistributorScripts();
                        objWorkDistributorScriptsAll.SQLScriptList = new List<string>();
                        string sqlscript = "";
                        
                        if(objWDExistingSizeDetailsEntity!=null)
                        {
                            if (objWDExistingSizeDetailsEntity.TableName != null)
                            {
                                intMaxval = intReqMaxVal;
                            }
                            else
                            {
                                intMaxval = intMaxTemstore;
                            }
                        }
                        else
                        {
                            intMaxval = intMaxTemstore;
                        }

                        intIncr = intIncr + intMaxval;

                        if (intIncr < intTRwsCount)
                        {
                            //i = 0;
                            //Generate Sql Script using Min and Max val;
                            sqlscript = " SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from " + objWorkDistributorEntity.TableName + ") AS TableNameScript " +
                                              " WHERE RowNum>=" + intMinVal + " and RowNum<= " + intIncr + "";

                            if (objWDExistingSizeDetailsEntity != null)
                            {
                                if (objWDExistingSizeDetailsEntity.TableName != null)
                                {
                                    objWorkDistributorScriptsAll.SQLScriptList.Add(objWDExistingSizeDetailsEntity.StrExistingSQLScript);
                                    intMinVal = intIncr + 1;
                                    intMaxval = 0;
                                    //intIncr = 0;
                                    //i = intMinVal + CalculateRequiredRowsFromTable(intTRS, intERwSize);
                                }
                            }
                            
                            objWDExistingSizeDetailsEntity = null;
                            //intMaxval= CalculateRequiredRowsFromTable(intTRS, intERwSize);
                            objWorkDistributorScriptsAll.SQLScriptList.Add(sqlscript);
                            strSqlScriptsList.Add(objWorkDistributorScriptsAll);
                            intslno = intslno + 1;
                            objWorkDistributorScriptsAll.Slno = intslno;
                        }
                        else
                        {
                            //Calculate Rest of Table Size
                            if (objWDExistingSizeDetailsEntity != null)
                            {
                                if (objWDExistingSizeDetailsEntity.TableName != null)
                                {
                                    objWorkDistributorScriptsAll.SQLScriptList.Add(objWDExistingSizeDetailsEntity.StrExistingSQLScript);
                                    strSqlScriptsList.Add(objWorkDistributorScriptsAll);
                                    intslno = intslno + 1;
                                    objWorkDistributorScriptsAll.Slno = intslno;
                                    objWDExistingSizeDetailsEntity = null;
                                }
                            }
                            objWDExistingSizeDetailsEntity = CalculateRestTotalRowsSize(objWorkDistributorEntity.TableName, intMinVal, intTRwsCount, intERwSize);

                        }
                        
                    }
                    
                }
                else
                {
                    //Generate Select * from all the records from table.
                    string sqlscript = " SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from " + objWorkDistributorEntity.TableName + ") AS TableNameScript ";
                    objWorkDistributorScriptsAll = new WorkDistributorScripts();
                    objWorkDistributorScriptsAll.SQLScriptList = new List<string>();
                    objWorkDistributorScriptsAll.SQLScriptList.Add(sqlscript);
                    strSqlScriptsList.Add(objWorkDistributorScriptsAll);
                    intslno = intslno + 1;
                    objWorkDistributorScriptsAll.Slno = intslno;
                }

                //strSqlScriptsList.Add(objWorkDistributorScriptsAll);
            }
            if (objWDExistingSizeDetailsEntity != null)
            {
                if (objWDExistingSizeDetailsEntity.TableName != null)
                {
                    WorkDistributorScripts objWorkDistributorScriptsAll = new WorkDistributorScripts();
                    objWorkDistributorScriptsAll.SQLScriptList = new List<string>();
                    objWorkDistributorScriptsAll.SQLScriptList.Add(objWDExistingSizeDetailsEntity.StrExistingSQLScript);
                    strSqlScriptsList.Add(objWorkDistributorScriptsAll);
                    intslno = intslno + 1;
                    objWorkDistributorScriptsAll.Slno = intslno;
                    objWDExistingSizeDetailsEntity = null;
                }
            }
            return strSqlScriptsList;
        }

        public static string GenerateSQLScriptThroughQueries(Int64 intRequiredSize, WDFileFormatInputs objWDFileFormatInputs)
        {
            List<WorkDistributorScripts> objWorkDistributorScripts = new List<WorkDistributorScripts>();
            string strFileOutput = "";
            string StrQuery = " DECLARE @TempTableName Table (intslno int identity(1,1),txtTableName Varchar(100))  " +
                             " DECLARE @TableSpaceUsed Table(intslno int identity(1, 1), txtName Varchar(100), intRowcount BIGInt, txtReserved Varchar(MAX), txtData Varchar(MAX),  " +
                             " txtIndexSize Varchar(Max), txtUnused Varchar(MAX))  " +

                                 " DECLARE @TablerowData Table(intslno int identity(1, 1), txtName Varchar(100), intRowcount BIGInt, txtReserved Varchar(MAX), txtData BIGINT,  " +
                                 " intEachRowSize Int,intRequriedRowsset INT,flgstatus bit,intRowsCoverd BIGINT,intRemaingRowstoCover BIGINT,intNoofSelectQry BIGINT,  " +
                                 " intRemaingSizetoCover BigINT,intRemaingRowsSize BigInt)  " +

                             " DECLARE @intRequiredSize BigInt=" + intRequiredSize +"" +

                             " Declare @TMPLooptable Table (intslno int identity(1,1), Script VARCHAR(MAX),intset INT,TableName VARCHAR(100),MinVal BIGINT,MaxVal BIGINT)  " +
                             " DECLARE @TmpColumnData Table (intslno int identity(1,1),TABLENAME VARCHAR(100),COLUMNNAME VARCHAR(100),DATATYPE VARCHAR(100),ISIDENTITY CHAR(1)) " +

                             " Insert into @TempTableName  " +
                             " select name from sys.tables  " +

                             " Declare @intRowcount int  " +
                             " Declare @intcountincr int= 0  " +

                             " Select @intRowcount = Count(1) from @TempTableName  " +

                             " WHILE @intcountincr < @intRowcount  " +
                             " BEGIN  " +
                             " SET @intcountincr = @intcountincr + 1  " +
                             " Declare @TableName Varchar(100)  " +
                             " SELECT @TableName = txtTableName FROM @TempTableName Where intslno = @intcountincr  " +

                             " Insert INTO @TableSpaceUsed  " +
                             " EXEC sp_spaceused @TableName  " +
                             " INSERT INTO @TmpColumnData "+
                             " SELECT @TableName,COLUMN_NAME, DATA_TYPE,'IsIdentity'= CASE COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') " +
                             " WHEN  1 then 'Y' ELSE 'N' END FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=@TableName" +
                             " END  " +


                            " Insert INTO @TablerowData  " +
                            " SELECT txtName AS 'Name',intRowcount AS 'Row Count', txtData AS 'Data KB',  " +
                            " (Substring(txtData, 1, len(txtData) - 2) * 1000) AS 'Data Byte',((Substring(txtData, 1, len(txtData) - 2) * 1000))/intRowcount AS 'Each Row Size Byte',  " +
                            " 0,0,0,intRowcount,0,0,0 FROM @TableSpaceUsed  " +

                            " Update @TablerowData set intRequriedRowsset=@intRequiredSize/intEachRowSize where 1=1  " +
                            " Update @TablerowData set intNoofSelectQry=intRowcount/intRequriedRowsset where 1=1  " +
                            " Update @TablerowData SET intRowsCoverd=intRequriedRowsset*intNoofSelectQry where 1=1  " +
                            " Update @TablerowData SET intRemaingRowstoCover=intRowcount-intRowsCoverd where 1=1  " +

                            " Update @TablerowData SET intRemaingSizetoCover=@intRequiredSize-(intRemaingRowstoCover * intEachRowSize) where 1=1  " +

                            " Update @TablerowData SET intRemaingRowsSize=intRemaingRowstoCover*intEachRowSize where 1=1  " +
                            
                            " Declare @intTotalByteRequired BigInt  " +
                            " SELECT @intTotalByteRequired=SUM((Substring(txtData, 1, len(txtData) - 2) * 1000)) from @TableSpaceUsed  " +
                            " DECLARE @inttablerowdatacount BigInt  " +
                            " SELECT @inttablerowdatacount=Count(1) from @TablerowData  " +
                            " DECLARE @intcountincrnew BigInt= 0  " +


                            " DECLARE @Table Table (intslno int identity(1,1),txtName Varchar(100),txtMinVal INT,txtMaxVal INT)  " +

                                " DECLARE @intSizetoCover BIGINT =0  " +
                                " DECLARE @intnoofselectqry INT  " +
                                " DECLARE @intRemaingSizetoCover BIGINT  " +
                                " DECLARE @intRemaingRowstoCover BIGINT  " +
                                " DECLARE @intRowCountN BigINT  " +
                                " DECLARE @TableNameN VARCHAR(100)  " +
                                " DECLARE @intReqRowsSet Bigint  " +
                                " DECLARE @intRemainingRowsSize BigInt  " +
                                " DECLARE @intRowsCovered BIGINT=0  " +
                                " DECLARE @intEachRowSize INT  " +
                                " Declare @intSet INT = 0 " +
                            " WHILE @intcountincrnew<@inttablerowdatacount  " +
                            " BEGIN  " +
                              " Set @intcountincrnew=@intcountincrnew+1  " +

                                " SET @intSizetoCover=0  " +
                                " SET @intnoofselectqry =0  " +
                                " SET @intRemaingSizetoCover =0  " +
                                " SET @intRemaingRowstoCover =0  " +
                                " SET @intRowCountN =0  " +
                                " SET @TableNameN =''  " +
                                " SET @intReqRowsSet =0  " +
                                " SET @intRemainingRowsSize =0  " +
                                " SET @intRowsCovered=0  " +
                                " SET @intEachRowSize=0  " +

                                " SELECT @TableNameN=txtname,@intRowCountN=introwcount, @intSizetoCover=intRemaingRowstoCover,@intnoofselectqry=intNoofSelectQry,  " +
                                " @intRemaingSizetoCover=intRemaingSizetoCover,  " +
                                " @intRemaingRowstoCover=intRemaingRowstoCover,@intReqRowsSet=intRequriedRowsset,@intRemainingRowsSize=intRemaingRowsSize,@intRowsCovered=intRowsCoverd  " +
                                " FROM @TablerowData WHERE intslno=@intcountincrnew  " +

                                " Declare @intCr INT=0  " +
                                " Declare @intMinVal VARCHAR(MAX)=1  " +
                                " Declare @intMaxVal VARCHAR(MAX)  " +
                                " DECLARE @txtQuery VARCHAR(MAX)  " +
                                " IF @intnoofselectqry>0  " +
                                " BEGIN  " +
                                  " WHILE @intCr<@intnoofselectqry  " +
                                  " BEGIN  " +
                                    " SET @intCr=@intCr+1  " +
                                    " SET @intMaxVal=@intMinVal + @intReqRowsSet  " +

                                    " IF @intCr = @intnoofselectqry " +
                                    " SET @intMaxVal = @intRowsCovered " +

                                    " SET @txtQuery ='SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from ' + @TableNameN + ' ) AS TableNameScript  " +
                                    " WHERE RowNum >= ' + @intMinVal +' and RowNum <= ' + @intMaxVal  " +


                                    " SET @intSet = @intSet + 1 " +
                                    " INSERT INTO @TMPLooptable VALUES (@txtQuery,@intSet,@TableNameN,@intMinVal,@intMaxVal)  " +

                                    " SET @intMinVal=CAST(@intMaxVal AS INT) +1  " +
                                  " END  " +
                                " END  " +
                            " END  " +




                            " SET @intcountincrnew=0  " +
                            " DECLARE @intSUMofRemainingRowSize BIGINT=0  " +

                             " SET @intSet = @intSet + 1 " +
                            " INSERT INTO @TMPLooptable VALUES ('',0,'',0,0)  " +
                            " WHILE @intcountincrnew<@inttablerowdatacount  " +
                            " BEGIN  " +
                              " Set @intcountincrnew=@intcountincrnew+1  " +
                              " SET @intMinVal =0  " +
                              " SET @intMaxVal =0  " +
                              " DECLARE @intRemainingRowstoCoverCR BIGINT  " +
                              " DECLARE @intRowsCoveredCR BIGINT  " +
                              " DECLARE @intRemainingSizetoCoverCR BIGINT   " +
                              " DECLARE @intRemainingRowsSizeCR BIGINT   " +
                              " DECLARE @intRowCountCR BIGINT  " +
                              " DECLARE @intEachRowSizeCR INT  " +
                              " DECLARE @TableNameCR VARCHAR(100)  " +

                                " SELECT @TableNameCR=txtname, @intRowCountCR=introwcount, @intRemainingRowstoCoverCR=intRemaingRowstoCover,  " +
                                " @intRemainingSizetoCoverCR=intRemaingSizetoCover,@intRemainingRowsSizeCR=intRemaingRowsSize,@intRowsCoveredCR=intRowsCoverd,@intEachRowSizeCR=intEachRowSize  " +
                                " FROM @TablerowData WHERE intslno=@intcountincrnew  " +

                                " SET @intSUMofRemainingRowSize=@intSUMofRemainingRowSize+@intRemainingRowsSizeCR  " +

                                " IF @intSUMofRemainingRowSize < @intRequiredSize  " +
                                " BEGIN  " +
                                  " SET @intMinVal=@intRowsCoveredCR+1  " +
                                  " SET @intMaxVal=@intRowCountCR  " +

                                  " SET @txtQuery= 'SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from ' + @TableNameCR + ') AS TableNameScript   " +
                                          " WHERE RowNum >=' + @intMinVal + ' and RowNum <=' + @intMaxVal   " +

                                  " UPDATE @TablerowData SET flgstatus=1,intRemaingRowstoCover=0,intRemaingSizetoCover=0,intRemaingRowsSize=0 WHERE intslno=@intcountincrnew  " +
                                  " INSERT INTO @TMPLooptable VALUES (@txtQuery,@intSet,@TableNameCR,@intMinVal,@intMaxVal)  " +

                                " END  " +
                                " ELSE  " +
                                " BEGIN  " +
                                  " SET @intSUMofRemainingRowSize=@intSUMofRemainingRowSize-@intRemainingRowsSizeCR  " +
                                  " DECLARE @intRequiredRowSizeFROMCR BIGINT= @intRequiredSize - @intSUMofRemainingRowSize  " +
                                  " DECLARE @intRequiredRowFROMCR BIGINT = @intRequiredRowSizeFROMCR/@intEachRowSizeCR  " +


                                  " UPDATE @TablerowData SET intRemaingRowstoCover=(intRemaingRowstoCover-@intRequiredRowFROMCR) WHERE intslno=@intcountincrnew  " +
                                  " UPDATE @TablerowData SET intRemaingRowsSize=intRemaingRowsSize-@intRequiredRowSizeFROMCR  WHERE intslno=@intcountincrnew  " +
                                  " UPDATE @TablerowData SET @intRemainingSizetoCoverCR=@intRequiredSize-intRemaingRowsSize WHERE intslno=@intcountincrnew  " +


                                  " SET @intMinVal=@intRowsCoveredCR+1  " +
                                  " SET @intMaxVal=@intRequiredRowFROMCR + @intRowsCoveredCR + 1  " +

                                  " SET @txtQuery= 'SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY (select null)) AS RowNum from ' + @TableNameCR + ') AS TableNameScript   " +
                                          " WHERE RowNum >=' + @intMinVal + ' and RowNum <=' + @intMaxVal   " +

                                  " UPDATE @TablerowData SET intRowsCoverd=@intMaxVal WHERE intslno=@intcountincrnew  " +

                                  " INSERT INTO @TMPLooptable VALUES (@txtQuery,@intSet,@TableNameCR,@intMinVal,@intMaxVal)  " +
                                  " Set @intcountincrnew=@intcountincrnew-1  " +
                                  " SET @intSet = @intSet + 1 " +
                                " END  " +
                                " IF @intSUMofRemainingRowSize+@intRequiredRowSizeFROMCR =@intRequiredSize  " +
                                " BEGIN  " +
                                  " SET @intSUMofRemainingRowSize=0  " +
                                  " INSERT INTO @TMPLooptable VALUES ('',0,'',0,0)  " +
                                " END  " +
                            " END  " +
                            " SELECT * FROM @TMPLooptable " +
                            " SELECT * FROM @TmpColumnData ";
            DataSet DS = new DataSet();
            DS = DataBaseUtility.DataSetText(StrQuery);

            int intincr = 1;
            List<int> i = (from row in DS.Tables[0].AsEnumerable()
                                  select row.Field<int>("intset")).Distinct().ToList();
            int intBatchNoStartValue = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["BatchNoStartValue"]);
            foreach (int list in i)
            {
                if(list>0)
                {
                   List<string> selectscript = (from row in DS.Tables[0].AsEnumerable()
                                    where row.Field<int>("intset") == list
                                    select row.Field<string>("Script")).ToList();

                    if (selectscript.Count == 1)
                    {

                        var RowDetails = (from row in DS.Tables[0].AsEnumerable()
                                               where row.Field<int>("intset") == list
                                               select new
                                               {
                                                   TableName= row.Field<string>("TableName"),
                                                   MinVal=row.Field<Int64>("MinVal"),
                                                   MaxVal = row.Field<Int64>("MaxVal"),
                                               });

                        List<WDFileFormatTableColumnDataInputs> WDFileFormatTableColumnDataInputsList = new List<WDFileFormatTableColumnDataInputs>();
                        var listnew = (from row in DS.Tables[1].AsEnumerable()
                                       where row.Field<string>("TABLENAME") == (RowDetails.First().TableName).ToString()
                                       select new
                                       {
                                           TABLENAME = row.Field<string>("TABLENAME"),
                                           COLUMNNAME = row.Field<string>("COLUMNNAME"),
                                           DATATYPE = row.Field<string>("DATATYPE"),
                                           ISIDENTITY = row.Field<string>("ISIDENTITY"),
                                       });

                        foreach (var WDFileFormatTableColumnDataInputsentity in listnew)
                        {
                            WDFileFormatTableColumnDataInputs objentity = new WDFileFormatTableColumnDataInputs();
                            objentity.COLUMNNAME = WDFileFormatTableColumnDataInputsentity.COLUMNNAME;
                            objentity.DATATYPE = WDFileFormatTableColumnDataInputsentity.DATATYPE;
                            objentity.TABLENAME = WDFileFormatTableColumnDataInputsentity.TABLENAME;
                            objentity.ISIDENTITY = WDFileFormatTableColumnDataInputsentity.ISIDENTITY;
                            WDFileFormatTableColumnDataInputsList.Add(objentity);
                        }

                        objWDFileFormatInputs.TABLENAME = RowDetails.First().TableName.ToString();
                        intBatchNoStartValue = intBatchNoStartValue + 1;

                        strFileOutput = FileOutputTemplate(objWDFileFormatInputs, Convert.ToInt64(RowDetails.First().MinVal), Convert.ToInt64(RowDetails.First().MaxVal), WDFileFormatTableColumnDataInputsList, intBatchNoStartValue);
                       
                    }
                    //WorkDistributorScripts objWorkDistributorScriptsEntity = new WorkDistributorScripts();
                    //objWorkDistributorScriptsEntity.SQLScriptList = selectscript;
                    //objWorkDistributorScripts.Add(objWorkDistributorScriptsEntity);
                }
            }
           
            return strFileOutput;

        }

        public static string FileOutputTemplate(WDFileFormatInputs objWDFileFormatInputs,Int64 intTableStartVal,Int64 intTableEndVal,List<WDFileFormatTableColumnDataInputs> TableDataList,int intBatchNoStartValue)
        {
            string strFileOutput = "";

            string strDataBaseAliasName = System.Configuration.ConfigurationManager.AppSettings["DataBaseAliasName"].ToString();
            string strDataBaseInstanceName = System.Configuration.ConfigurationManager.AppSettings["DataBaseInstanceName"].ToString();
            string strBatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"].ToString();
            
            string strAFFilePath = System.Configuration.ConfigurationManager.AppSettings["AFFilePath"].ToString();
            string strAFXFilePath = System.Configuration.ConfigurationManager.AppSettings["AFXFilePath"].ToString();

            //string strAFFileName = System.Configuration.ConfigurationManager.AppSettings["AFFileName"].ToString();
            //string strAFXFileName = System.Configuration.ConfigurationManager.AppSettings["AFXFileName"].ToString();

            string strDBObjectName= System.Configuration.ConfigurationManager.AppSettings["DBObjectName"].ToString();
            string strSchemaName = System.Configuration.ConfigurationManager.AppSettings["SchemaName"].ToString();
            string strColumnDataType = System.Configuration.ConfigurationManager.AppSettings["ColumnDataType"].ToString().ToUpper();

            string strServiceName = strDataBaseAliasName + strDataBaseInstanceName + strBatchName + intBatchNoStartValue;
            strFileNameOut = strDataBaseAliasName + strDataBaseInstanceName + strBatchName + intBatchNoStartValue;

            string strNATIVELOB = "N";
            string strColumns = "";
            string strISidentityColumn = "";

            strFileOutput = " Create ARCH " + strServiceName + "@@"+
                            " AF //'\\"+ strAFFilePath + "\\"+ strFileNameOut + ".AF' // AFX //'\\ " + strAFXFilePath+"\\"+ strFileNameOut + ".AFX'// @@" +
                            " ROWLIMIT 0 DBCONNECTIONS 1 DELDBCONNECTIONS 1 DEFER_DAA Y REVIEW_DELETE N DELCF N @@" +
                            " COMPRESSFILE " + objWDFileFormatInputs.COMPRESSFILE + " COMPRESSINDEXFILE "+ objWDFileFormatInputs.COMPRESSINDEXFILE + " COMPRESSMODEL " + objWDFileFormatInputs.COMPRESSMODEL + "  COMPRESSACTIVE " + objWDFileFormatInputs.COMPRESSACTIVE + " @@" +
                            " ENCRYPTAF "+ objWDFileFormatInputs .ENCRYPTAF + " ENCRYPTIDX "+ objWDFileFormatInputs.ENCRYPTIDX + " GENSTATISTIC "+ objWDFileFormatInputs.GENSTATISTIC + " PROCESS_FILEATTACH "+ objWDFileFormatInputs.PROCESS_FILEATTACH + " CREATEREPORT "+ objWDFileFormatInputs.CREATEREPORT + " @@" +
                            " "+ objWDFileFormatInputs.ACCESSDEFINATION + "( @@" +
                            " SRCQUAL "+ strDBObjectName + " START " + strSchemaName + objWDFileFormatInputs.TABLENAME + " ADDTBLS N MODCRIT N ADCHGS N USENEW Y USEFM N PNSSTATE N SOLUTION 0 @@" +
                            " VAR (START PRMPT //'START_VALUE'// DFLT "+ intTableStartVal + ") @@" +
                            " VAR (END PRMPT //'END_VALUE'// DFLT "+ intTableEndVal + ") @@" +
                            " TABLE ("+ strSchemaName + objWDFileFormatInputs.TABLENAME + " ACCESS SUID REF N DAA N UR N PREDOP A VARDELIM : COLFLAG N @@";

            foreach(WDFileFormatTableColumnDataInputs ObjWDFileFormatTableColumnDataInputs in TableDataList)
            {
                if(ObjWDFileFormatTableColumnDataInputs.TABLENAME.ToUpper().Trim()== objWDFileFormatInputs.TABLENAME.ToUpper().Trim())
                {
                    if(ObjWDFileFormatTableColumnDataInputs.DATATYPE.ToUpper().Trim().Contains(strColumnDataType))
                        strNATIVELOB = "Y";

                    if (ObjWDFileFormatTableColumnDataInputs.ISIDENTITY == "Y")
                        strISidentityColumn = ObjWDFileFormatTableColumnDataInputs.COLUMNNAME;

                     strColumns = strColumns + " COLUMN (" + ObjWDFileFormatTableColumnDataInputs.COLUMNNAME + " DISP Y ACCESS U HEADING NC NATIVELOB " + strNATIVELOB + " EXTRACT Y @@";
                }
            }
            strFileOutput = strFileOutput + strColumns;

            if(strISidentityColumn != "")
                strFileOutput = strFileOutput+ " SQL //'" + strISidentityColumn + " BETWEEN :START AND :END'// @@";
            else
                strFileOutput = strFileOutput + " SQL //'" + strISidentityColumn + " BETWEEN :START AND :END'// @@"; // Need to check.

            strFileOutput = strFileOutput + " EXTRROWID N ) @@" + // Need to check.
                            ") @@" +
                            " INCLPK Y  INCLFK Y  INCLIDX Y  INCLALIAS Y  INCLASSEMBLY Y  INCLFUNCTION Y  INCLPACKAGE Y  INCLPAR_FUNCTION Y  INCLPAR_SCHEME Y  INCLPROCEDURE Y  INCLSEQUENCE Y @@" +
                            " INCLTRIGGER Y  INCLVIEW Y  INCLDEF Y  INCLRULE Y  INCLUDT Y  INCLMETHOD Y PNSOVERRIDE N PNSOPT Y @@" +
                            " VARS //'START/1,END/5' // ALWAYSPROMPT N ALWAYSPROMPT N OBJQAL WDWV2_P2.dbo IGNOREUNKNOWN N @@" +
                            " DELETESTRATEGY(TABLE(" + strDBObjectName + strSchemaName + objWDFileFormatInputs.TABLENAME + " D 1));";
            strFileOutput = strFileOutput.Replace("@@", Environment.NewLine);
            FileSave(strFileNameOut, strFileOutput);
            return strFileOutput;
        }

        public static void FileSave(string strFileNameOut,string strData)
        {
            string strAFFilePath = "";
            string strAFXFilePath = "";
            string strSaveLocal = System.Configuration.ConfigurationManager.AppSettings["SaveLocal"];
            //string strAFFileName = "";
            //string strAFXFileName = "";

            if (strSaveLocal == "Y")
            {
                strAFFilePath = System.Configuration.ConfigurationManager.AppSettings["LocalFilesavepath"];
                strAFXFilePath = strAFFilePath;
                strFileNameOut = strFileNameOut + ".txt";
                //File.Create(strAFFilePath + strFileNameOut);

            }
            else
            {
                strAFFilePath = System.Configuration.ConfigurationManager.AppSettings["AFFilePath"];
                strAFXFilePath = System.Configuration.ConfigurationManager.AppSettings["AFXFilePath"];
                strFileNameOut = strFileNameOut +".AF";
                //File.Create(strAFFilePath + "\\" + strFileNameOut);
                //File.Create(strAFXFilePath + "\\" + strFileNameOut.Replace(".AF", ".AFX"));
            }

            StreamWriter sw = null;
            if (strSaveLocal == "Y")
            {
                sw = new StreamWriter(strAFFilePath + strFileNameOut);
                sw.WriteLine(strData);
            }
            else
            {
                sw = new StreamWriter(strAFFilePath + strFileNameOut);
                sw.WriteLine(strData);
                sw.Close();
                sw.Dispose();
                sw = new StreamWriter(strAFXFilePath + strFileNameOut.Replace(".AF",".AFX"));
                sw.WriteLine(strData);
            }
            sw.Close();
            sw.Dispose();
        }
    }
}
