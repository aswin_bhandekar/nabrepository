﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace WorkDistributorBL
{
   public class DataBaseUtility
    {
        public static DataSet DataSet(string strSPName, params object[] parameterValues)
        {
            DataSet Ds = new DataSet();
            SqlCommand sqlcmd = SQLCmd(strSPName, parameterValues);
            SqlDataAdapter sqladp = new SqlDataAdapter(sqlcmd);
            sqladp.Fill(Ds);
            return Ds;
        }
        public static SqlConnection SQLCON()
        {
            string strsqlconn = ConfigurationManager.ConnectionStrings["SQLDBCon"].ConnectionString;
            SqlConnection SQLConn = new SqlConnection(strsqlconn);
            if (SQLConn.State == ConnectionState.Closed)
            {
                SQLConn.Open();
            }
            return SQLConn;
        }
        public static int ExecuteNonQuery(string strSPName, params object[] parameterValues)
        {
            int intsuccess;
            try
            {
                SqlConnection sqlconn = SQLCON();
                SqlCommand sqlcmd = SQLCmd(strSPName, parameterValues);
                sqlcmd.ExecuteNonQuery();
                sqlconn.Close();
                intsuccess = 1;
            }
            catch (Exception ex)
            {
                intsuccess = 0;
            }
            return intsuccess;
        }
        public static SqlCommand SQLCmd(string strSPName, params object[] parameterValues)
        {
            SqlConnection sqlconn = SQLCON();
            SqlCommand sqlcmd = new SqlCommand();
            try
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = strSPName;
                sqlcmd.Connection = sqlconn;
                if(parameterValues !=null)
                {
                    foreach (SqlParameter sqlparam in parameterValues)
                    {
                        sqlcmd.Parameters.Add(sqlparam.ParameterName, sqlparam.SqlDbType, sqlparam.Size).Value = sqlparam.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sqlcmd;
        }

        public static SqlCommand SQLCmdText(string strQuery)
        {
            SqlConnection sqlconn = SQLCON();
            SqlCommand sqlcmd = new SqlCommand();
            try
            {
                sqlcmd.CommandType = CommandType.Text;
                sqlcmd.CommandText = strQuery;
                sqlcmd.Connection = sqlconn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sqlcmd;
        }

        public static DataSet DataSetText(string strSPName)
        {
            DataSet Ds = new DataSet();
            SqlCommand sqlcmd = SQLCmdText(strSPName);
            SqlDataAdapter sqladp = new SqlDataAdapter(sqlcmd);
            sqladp.Fill(Ds);
            return Ds;
        }
    }
}
