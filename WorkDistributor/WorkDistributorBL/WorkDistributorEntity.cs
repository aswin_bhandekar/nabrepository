﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkDistributorBL
{
    public class WorkDistributorEntity
    {
        public string TableName { get; set; }
        public decimal TableSizeMB { get; set; }
        public decimal TableSizeKB { get; set; }
        public Int64 TableSizeByt { get; set; }
        public Int64 TableRowCount { get; set; }

       
    }

    public class WorkDistributorScripts
    {
        //public string TableNames { get; set; }

        public Int64 Slno { get; set; }
        //public Int64 TotalRowCount { get; set; }
        //public Int64 RowsEffected { get; set; }
        public List<string> SQLScriptList { get; set; }
    }

    public class WDRowsDetails
    {
        public Int64 IntRowCount { get; set; }
        public Int64 IntEachRowSizeByt { get; set; }
        public Int64 IntRowsforSplitSize { get; set; }
    }

    public class WDRestRowsDetails
    {
        public Int64 IntRemainingRows { get; set; }
        public Int64 IntRemainingRowsSize { get; set; }
        public Int64 IntSizeNeededFormNextTable { get; set; }
    }
    public class WDExistingSizeDetails
    {
        public Int64 IntExistingSize { get; set; }
        public Int64 IntExistingMinVal { get; set; }
        public Int64 IntExistingRowscount { get; set; }
        public string TableName { get; set; }
        public string StrExistingSQLScript { get; set; }
    }
    public class WDFileFormatInputs
    {
        public string COMPRESSFILE { get; set; }
        public string COMPRESSINDEXFILE { get; set; }
        public int COMPRESSMODEL { get; set; }
        public string COMPRESSACTIVE { get; set; }
        public string ENCRYPTAF { get; set; }
        public string ENCRYPTIDX { get; set; }
        public string GENSTATISTIC { get; set; }
        public string PROCESS_FILEATTACH { get; set; }
        public string CREATEREPORT { get; set; }
        public string ACCESSDEFINATION { get; set; }
        public string TABLENAME { get; set; }
        
    }

    public class WDFileFormatTableColumnDataInputs
    {
        public string TABLENAME { get; set; }
        public string COLUMNNAME { get; set; }
        public string DATATYPE { get; set; }
        public string ISIDENTITY { get; set; }
    }
}
